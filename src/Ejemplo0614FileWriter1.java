
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/*
 Aplicacion que escribe en un fichero
 una palabra y despues la lee.
 */
/**
 * @date 20-ene-2015 Fichero Ejemplo0614a.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejemplo0614FileWriter1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        String f = "Ejemplo0614FileWriter1";

        FileWriter fw = new FileWriter(f);
        try {
            fw.write("prueba");
        } catch (IOException ex) {
        } finally {
            fw.close();
        }

        FileReader fr = new FileReader(f);
        BufferedReader br = new BufferedReader(fr);
        try {
            //char c= (char) br.read();
            //System.out.println( c );
            System.out.println(br.readLine());
        } catch (IOException ex) {
        } finally {
            fw.close();
        }
    }
}
