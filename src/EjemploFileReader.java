
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringTokenizer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * Fichero: EjemploFileReader.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 25-ene-2016
 */
public class EjemploFileReader {

    public static void main(String[] args) throws IOException {

        FileWriter fw = new FileWriter("EjemploFileReader.txt");
        fw.write("id1;paco\n");
        fw.write("id2;juan\n");
        fw.close();

        FileReader fr = new FileReader("EjemploFileReader.txt");
        BufferedReader br = new BufferedReader(fr);
        //char c = (char) fr.read();
        String linea = br.readLine();
        while (linea != null) {
            StringTokenizer st = new StringTokenizer(linea, ";");
            String id = st.nextToken();
            String nombre = st.nextToken();
            System.out.println(id + " " + nombre);
            linea = br.readLine();
        }

        //System.out.println(linea);
    }

}
