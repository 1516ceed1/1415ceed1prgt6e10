
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.logging.Logger;

/**
 *
 * @author Paco Aldarias<paco.aldarias@ceedcv.es>
 */
public class FileWriterUpdate {

    static void update(Alumno alumno) throws FileNotFoundException, IOException {
        File origen = new File("alumno.csv");
        File destino = new File("temp.csv");

        FileReader fr = new FileReader(origen);
        FileWriter fw = new FileWriter(destino);
        BufferedReader br = new BufferedReader(fr);

        String linea = br.readLine();
        StringTokenizer st;
        String id;
        String nombre;

        while (linea != null) {
            st = new StringTokenizer(linea, ";");
            id = st.nextToken();
            nombre = st.nextToken();
            if (alumno.getId() == Integer.parseInt(id)) {
                fw.write(alumno.getId() + ";" + alumno.getNombre() + ";\n");
            } else {
                fw.write(id + ";" + nombre + ";\n");
            }
            linea = br.readLine();
        }
        fw.close();;
        fr.close();

        origen.delete();
        destino.renameTo(origen);
    }

    public static void main(String[] args) throws IOException {

        FileWriter fw = new FileWriter("alumno.csv");
        fw.write("0;Paco;\n");
        fw.write("1;Pepe;\n");
        fw.write("2;Fran;\n");
        fw.close();

        Alumno alumno = new Alumno();
        alumno.setId(1);
        alumno.setNombre("Cindi");
        update(alumno);

    }

}

class Alumno {

    private int id;
    private String nombre;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
