/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Fichero: Ejemplo0611Descargahtml.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-ene-2014
 */
public class Ejemplo0611Descargahtml {

  /**
   * Descarga un fichero html y lo guarda en web.html
   *
   * @param args
   */
  public static void main(String[] args) {
    try {
      // Url con la foto
      URL url = new URL("http://www.eltiempo.es/valencia.html");
      // establecemos conexion
      URLConnection urlCon = url.openConnection();
      // Sacamos por pantalla el tipo de fichero
      System.out.println(urlCon.getContentType());
      // Se obtiene el inputStream de la web y se abre el fichero
      // local.
      InputStream is = urlCon.getInputStream();
      FileOutputStream fos = new FileOutputStream("web.html");
      // Lectura de la foto de la web y escritura en fichero local
      byte[] array = new byte[1000]; // buffer temporal de lectura.
      int leido = is.read(array);
      while (leido > 0) {
        fos.write(array, 0, leido);
        leido = is.read(array);
      }
      // cierre de conexion y fichero.
      is.close();
      fos.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
