
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paco Aldarias<paco.aldarias@ceedcv.es>
 */
public class FileWriterHolaMundo {

    public static void main(String[] args) throws IOException {
        int iletra;
        try {
            FileWriter fr = new FileWriter("file.txt");
            String texto = "Hola Mundo";
            for (int i = 0; i < texto.length(); i++) {
                fr.write(texto.charAt(i) + "");
            }
            fr.close();
        } catch (Exception e) {

        }

        try {
            FileReader fr = new FileReader("file.txt");
            BufferedReader br = new BufferedReader(fr);
            iletra = fr.read();
            while (iletra != -1) {
                System.out.print((char) iletra);
                iletra = fr.read();
            }
            System.out.println("");
            fr.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileWriterHolaMundo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
/* Ejecucion
 Hola Mundo!
 */
