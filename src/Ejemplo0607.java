/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Fichero: Ejemplo0607.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-ene-2014
 */
public class Ejemplo0607 {

  public static void main(String args[]) throws IOException {
    PrintWriter pantalla = new PrintWriter(System.out);
    char[] array = {'A', 'l', 'd', 'a', 'r', 'i', 'a', 's'};
    String str = new String("Paco");
    pantalla.write(str);
    pantalla.print(" ");
    pantalla.write(array, 0, 3);
    pantalla.println("");
    pantalla.flush();
  }
}
/* EJECUCION:
 Paco Ald
 */
