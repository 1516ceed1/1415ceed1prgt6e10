
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paco Aldarias<paco.aldarias@ceedcv.es>
 */
public class FileOutputStreamHolaMundo {

    public static void main(String[] args) throws IOException {

        int iletra;
        try {
            FileOutputStream fos = new FileOutputStream("file.bin");
            String texto = "Hola Mundo";
            for (int i = 0; i < texto.length(); i++) {
                fos.write(texto.charAt(i));
            }
            fos.close();
        } catch (Exception e) {

        }

        try {
            FileInputStream fis = new FileInputStream("file.bin");
            String linea;
            iletra = fis.read();
            while (iletra != -1) {
                System.out.print((char) iletra);
                iletra = fis.read();
            }
            System.out.println("");
            fis.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Error: No existe fichero");
        }

    }

}
/* Ejecucion
 Hola Mundo!
 */
