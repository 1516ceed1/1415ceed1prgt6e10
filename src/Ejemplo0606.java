/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Fichero: Ejemplo0606.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-ene-2014
 */
public class Ejemplo0606 {

  public static void main(String[] args) {
    InputStreamReader inputStreamReader = new InputStreamReader(System.in);
    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
    try {
      System.out.print("Sumando 1 : ");
      int s1 = Integer.parseInt(bufferedReader.readLine());
      System.out.print("Sumando 2 : ");
      int s2 = Integer.parseInt(bufferedReader.readLine());
      int suma = s1 + s2;
      System.out.println("La suma es " + s1 + "+" + s2 + "=" + suma);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}

/* EJECUCION
 Sumando 1 : 2
 Sumando 2 : 3
 La suma es 2+3=5
 */
